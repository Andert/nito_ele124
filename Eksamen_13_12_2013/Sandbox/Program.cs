﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomClass classA = new CustomClass(100);
            CustomClass classB = classA;
            classA.Value = 200;

            Console.WriteLine($"ClassA: {classA}, ClassB:{classB}");

            CustomStruct structA = new CustomStruct(100);
            CustomStruct structB = structA;
            structA.Value = 200;

            Console.WriteLine($"StructA: {structA}, StructB:{structB}");
        }

        class CustomClass
        {
            public CustomClass(int i)
            {
                this.Value = i;
            }
            public int Value { get; set; }
            public override string ToString()
            {
                return $"This Class has value: {Value}";
            }
        }

        struct CustomStruct
        {
            public CustomStruct(int i)
            {
                this.Value = i;
            }
            public int Value { get; set; }
            public override string ToString()
            {
                return $"This Struct has value: {Value}";
            }
        }
    }
}
