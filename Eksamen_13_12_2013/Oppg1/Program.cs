﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oppg1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Contestant> contestants = new List<Contestant>();
            contestants.Add(new Contestant("Ole", "BergenIL"));
            contestants.Add(new Contestant("Per", "StavangerIL"));
            contestants.Add(new Contestant("Pål", "BodøIL"));

            GivePoints(contestants);

            contestants.Sort(MyCustomSort);

            foreach (Contestant person in contestants)
            {
                Console.WriteLine(person.ToString());
            }
        }

        private static int MyCustomSort(Contestant x, Contestant y)
        {
            return (int)(y.Points.GetCombinedScore() - x.Points.GetCombinedScore());
        }

        private static void GivePoints(List<Contestant> contestants)
        {
            foreach (Contestant contestant in contestants)
            {
                Console.WriteLine($"Give points to {contestant.Name}");
                for (int i = 0; i < 7; i++)
                {
                    double pointTemp = Double.Parse(Console.ReadLine());
                    contestant.AddPoint(pointTemp);
                }
            }
        }
    }

    class Contestant
    {
        public string Name { get; }
        public Points Points { get; }
        string club;

        public Contestant(string name, string club)
        {
            this.Name = name;
            this.club = club;
            this.Points = new Points();
        }

        public void AddPoint(double point)
        {
            //TODO:Check that point <10 etc...
            Points.PointList.Add(point);
        }

        public override string ToString()
        {
            return $"Name:{this.Name}, Club:{this.club}, Points:{this.Points}";
        }
    }

    internal class Points
    {
        public List<double> PointList { get; private set; }

        public Points()
        {
            PointList = new List<double>();
        }

        public double GetCombinedScore()
        {
            //TODO: check pointlist.count >=7
            PointList.Sort();
            double average = GetSumMinusTopAndButtomValue() / (PointList.Count - 2);
            return average;
        }

        private double GetSumMinusTopAndButtomValue()
        {
            double sum = 0;
            for (int i = 1; i < PointList.Count - 1; i++)
            {
                sum += PointList[i];
            }
            return sum;
        }

        public override string ToString()
        {
            string returnString = "";
            returnString += $"CombinedScore: {this.GetCombinedScore()}, Induvidal points: ";
            foreach ( double p in PointList)
            {
                returnString += p.ToString() + ", ";
            }
            returnString.Trim(',');
            return returnString;
        }
    }
}
