﻿namespace Oppg5
{
    public class Tollstation
    {
        public string Place { get; }
        public int Id { get; }

        public Tollstation(string place, int id)
        {
            this.Place = place;
            this.Id = id;
        }

        public override string ToString()
        {
            return Place;
        }
    }
}