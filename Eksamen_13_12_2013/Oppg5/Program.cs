﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oppg5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Tollstation> myTollstations = new List<Tollstation>();
            myTollstations.Add(new Tollstation("Bergen", 66));
            myTollstations.Add(new Tollstation("Tromsø", 44));

            Car myCar = new Car("soiergjs");
            Car yourCar = new CarWAutoPass("testso", 99);

            List<Passing> myPassings = new List<Passing>();
            myPassings.Add(new Passing(myTollstations[0], myCar));
            myPassings.Add(new PassingWOutAutoPass("picture0001.jpg",myTollstations[1], yourCar));
            myPassings.Add(new PassingWOutAutoPass("picture0001.jpg", myTollstations[0], yourCar));
            myPassings.Add(new Passing(myTollstations[1], myCar));

            foreach (var passing in myPassings)
            {
                Console.WriteLine(passing.ToString());
            }
        }
    }
}
