﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oppg5
{
    public class Car
    {
        string RegNr { get; }
        public Car(string regNr)
        {
            this.RegNr = regNr;
        }

        public override string ToString()
        {
            return $"regnr:{this.RegNr}";
        }
    }

    public class CarWAutoPass : Car
    {
        int Id { get; }

        public CarWAutoPass(string regNr, int id) : base(regNr)
        {
            this.Id = id;
        }
        public override string ToString()
        {
            return base.ToString() + $" Autopass{this.Id}";
        }
    }
}