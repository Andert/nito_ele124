﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CustomClassLib;

namespace Oppg6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Passing> myPassings = new List<Passing>();
        public MainWindow()
        {
            List<Tollstation> myTollstations = new List<Tollstation>();
            myTollstations.Add(new Tollstation("Bergen", 66));
            myTollstations.Add(new Tollstation("Tromsø", 44));
            SPecialClass myObj = new SPecialClass();
            List<Car> myCars = new List<Car>();
            myCars.Add(new Car("awoefijaw"));
            myCars.Add(new Car("RK3333"));
            myCars.Add(new CarWAutoPass("sk3333", 90));
            myCars.Add(new CarWAutoPass("RV000222", 66));

            InitializeComponent();
            tollListBox.ItemsSource = myTollstations;
            carListBox.ItemsSource = myCars;
            AddButton.Click += AddButtonClicked;
        }

        private void AddButtonClicked(object sender, RoutedEventArgs e)
        {
            if (tollListBox.SelectedItem != null && carListBox.SelectedItem != null)
            {
                myPassings.Add(new Passing((Tollstation)tollListBox.SelectedItem, (Car)carListBox.SelectedItem));
                PassingListBox.ItemsSource = null;
                PassingListBox.ItemsSource = myPassings;
            }
        }
    }
}
