﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oppg4
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = File.ReadAllLines(@"../../oppg4-start.txt");
            List<DataLine> rawData = new List<DataLine>();
            rawData = ConvertLinesToData(lines);
            Console.WriteLine($"Lines exists:{lines.Length}, Lines Converted:{rawData.Count}");
            try
            {
                CheckDataForErrors(rawData);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Exception happend: "+ex.Message);
            }

            WriteDataToFile(rawData);
        }

        private static void CheckDataForErrors(List<DataLine> rawData)
        {
            for (int i = 1; i < rawData.Count; i++)
            {
                if (Math.Abs(rawData[i].Value - rawData[i - 1].Value) > 2)
                {
                    throw new Exception("Too big difference");
                }
            }
        }

        private static void WriteDataToFile(List<DataLine> rawData)
        {
            List<string> toFile = new List<string>();
            for (int i = 1; i < rawData.Count; i++)
            {
                toFile.Add($"{rawData[i].Time} Diff:{rawData[i].Value - rawData[i - 1].Value}");
            }
            File.WriteAllLines(@"../../oppg4.txt", toFile.ToArray());
        }

        private static List<DataLine> ConvertLinesToData(string[] lines)
        {
            List<DataLine> rawData = new List<DataLine>();

            foreach (string line in lines)
            {
                string[] splittedLine = line.Split(' ');
                double tempValue;
                if (double.TryParse(splittedLine[1], out tempValue))
                {
                    rawData.Add(new DataLine(splittedLine[0], tempValue));
                }
            }
            return rawData;
        }

        struct DataLine
        {
            public string Time { get; set; }
            public double Value { get; set; }

            public DataLine(string time, double value)
            {
                this.Time = time;
                this.Value = value;
            }
        }
    }
}
