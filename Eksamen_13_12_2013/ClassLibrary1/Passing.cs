﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomClassLib
{
    public class Passing
    {
        public DateTime TimeAtPassing { get; }
        public Tollstation Tollstation { get; }
        public Car Car { get; }
        public Passing(Tollstation tollstation, Car car)
        {
            this.TimeAtPassing = DateTime.Now;
            this.Car = car;
            this.Tollstation = tollstation;
        }

        public override string ToString()
        {
            return $"{this.Car} passed {this.Tollstation} at time {this.TimeAtPassing}";
        }
    }

    public class PassingWOutAutoPass : Passing
    {
        public string RefFoto { get; }

        public PassingWOutAutoPass(string refFoto, Tollstation tollstation, Car car) : base(tollstation, car)
        {
            this.RefFoto = refFoto;
        }

        public override string ToString()
        {
            return base.ToString() + $" RefFoto:{this.RefFoto}";
        }
    }
}
