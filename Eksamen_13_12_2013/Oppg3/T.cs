﻿using System;

namespace Tid
{
    public class Tid
    {
        private int time;   // 0 - 23
        private int minutt; // 0 - 59
        private int sekund; // 0 - 59
        private string v;

        // ----  LEGG TIL DIN KODE HER.  ----

        public TimeSpan Time { get; private set; }

        public Tid(int hour=0, int minute=0, int second=0)
        {
            //if(hour<=24&&....) throw new ArgumetnException()
            this.Time = new TimeSpan(hour, minute, second);
        }

        public Tid(Tid copyTime)
        {
            this.Time = copyTime.Time;
        }

        public Tid(string now)
        {
            if (now == "nå")
            {
                Tid t = new Tid(Tid.Now());
                this.Time = t.Time;
            }
            else
            {
                throw new ArgumentException("Må skrive 'nå'");
            }
        }

        public override string ToString()
        {
            return this.Time.ToString();
        }

        public static Tid Now()
        {
            DateTime now = DateTime.Now;
            return new Tid(now.Hour, now.Minute, now.Second);
        }
    } 
}
