﻿using System;

namespace Tid
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Tid t1 = new Tid();             // 00:00:00 (standardverdier)
            Tid t2 = new Tid(2);            // 02:00:00
            Tid t3 = new Tid(21, 34);       // 21:34:00
            Tid t4 = new Tid(12, 25, 42);   // 12:25:42
            Tid t5 = new Tid(27, 74, 99);   // 00:00:00 (ugyldig, settes til standardverdier)
            Tid t6 = new Tid(t4);           // 12:25:42
            //Tid t7 = new Tid("nå");         // (nå)
            //Tid t7 = Tid.Now();
            Tid t7;
            string s = GetUserInput();
            try
            {
                t7 = new Tid(s);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception : {ex.Message}");
            }

            Console.WriteLine("Test av konstruktører:\n");
            Console.WriteLine("t1: {0}  Alle argumenter standardverdi", t1.ToString());                     // 00:00:00
            Console.WriteLine("t2: {0}  Time angitt, minutt og sekund standardverdier.", t2.ToString());    // 02:00:00
            Console.WriteLine("t3: {0}  Time og minutt angitt, sekund standardverdi.", t3.ToString());      // 21:34:00
            Console.WriteLine("t4: {0}  Time, minutt og sekund angitt.", t4.ToString());                    // 12:25:42
            Console.WriteLine("t5: {0}  Kun ugyldige verdier angitt -> standardverdier.", t5.ToString());   // 00:00:00
            Console.WriteLine("t6: {0}  Tid-objekt t4 angitt.", t6.ToString());                             // 12:25:42
            //Console.WriteLine("t7: {0}  Nå.", t7.ToString());                                               // (nå)
            
            Console.WriteLine("\nTrykk en tast ...");
            Console.ReadKey();
        }

        private static string GetUserInput()
        {
            return Console.ReadLine();
        }
    } 
}

