﻿using System;

public class Ansatt
{
    static Ansatt()
    {
        Teller = 0;
    }
    public string Fornavn { get;}
    public string Etternavn { get;}
    public static int Teller { get; private set; }

    public Ansatt(string fn, string en)
    {
        Fornavn = fn;
        Etternavn = en;
        Teller++;
    }

    public override string ToString()
    {
        return $"{this.Fornavn} {this.Etternavn}";
    }
}

